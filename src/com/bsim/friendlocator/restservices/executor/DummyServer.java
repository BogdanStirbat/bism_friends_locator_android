package com.bsim.friendlocator.restservices.executor;

import java.util.ArrayList;
import java.util.List;

import com.bsim.friendlocator.data.FriendLocation;
import com.bsim.friendlocator.restservices.requestmessage.AcceptSession;
import com.bsim.friendlocator.restservices.requestmessage.CheckSession;
import com.bsim.friendlocator.restservices.requestmessage.CreateNewSession;
import com.bsim.friendlocator.restservices.requestmessage.DeleteSessionRequest;
import com.bsim.friendlocator.restservices.requestmessage.SendLocationRequest;
import com.bsim.friendlocator.restservices.requestmessage.UpdateMapRequest;
import com.bsim.friendlocator.restservices.responsemessage.AcceptSessionResponse;
import com.bsim.friendlocator.restservices.responsemessage.CheckSessionResponse;
import com.bsim.friendlocator.restservices.responsemessage.DeleteSessionResponse;
import com.bsim.friendlocator.restservices.responsemessage.NewSessionResponse;
import com.bsim.friendlocator.restservices.responsemessage.SendLocationResponse;
import com.bsim.friendlocator.restservices.responsemessage.UpdateMapResponse;

public class DummyServer implements Server{

	@Override
	public NewSessionResponse createNewSession(CreateNewSession createNewSession) {
		NewSessionResponse response = new NewSessionResponse();
		response.setSuccess(true);
		response.setSessionId("test_session_id");
		response.setMessage("session created successfully");
		return response;
	}

	@Override
	public CheckSessionResponse checkSession(CheckSession checkSession) {
		CheckSessionResponse response = new CheckSessionResponse();
		response.setHasSession(true);
		response.setSessionId("session_id");
		return response;
	}

	@Override
	public AcceptSessionResponse acceptSession(AcceptSession acceptSession) {
		AcceptSessionResponse response = new AcceptSessionResponse();
		response.setSuccess(true);
		response.setAditionalInfo("session accepted successfully");
		return response;
	}

	@Override
	public UpdateMapResponse updateMap(UpdateMapRequest updateMapRequest) {
		UpdateMapResponse response = new UpdateMapResponse();
		response.setSuccess(true);
		response.setAdditionalInfo("last data retrieved");
		List<FriendLocation> friends = new ArrayList<FriendLocation>();
		FriendLocation friend1 = new FriendLocation();
		friend1.setName("Name 1");
		friend1.setPhone("0740789601");
		friend1.setLatitude("44.428552");
		friend1.setLongitude("26.098140");
		FriendLocation friend2 = new FriendLocation();
		friend2.setName("Name 2");
		friend2.setPhone("0740789602");
		friend2.setLatitude("44.432457");
		friend2.setLongitude("26.119545");
		FriendLocation friend3 = new FriendLocation();
		friend3.setName("Name 3");
		friend3.setPhone("0740789603");
		friend3.setLatitude("44.442600");
		friend3.setLongitude("26.102850");
		friends.add(friend1);
		friends.add(friend2);
		friends.add(friend3);
		response.setFriends(friends);
		return response;
	}

	@Override
	public SendLocationResponse sendLocation(SendLocationRequest request) {
		SendLocationResponse response = new SendLocationResponse();
		response.setSuccess(true);
		response.setAditionalInfo("location was sent successfully");
		return response;
	}

	@Override
	public DeleteSessionResponse deleteSession(DeleteSessionRequest request) {
		DeleteSessionResponse response = new DeleteSessionResponse();
		response.setSuccess(true);
		response.setAditionalInfo("session deleted successfully");
		return response;
	}
}
