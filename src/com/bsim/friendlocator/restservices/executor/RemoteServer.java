package com.bsim.friendlocator.restservices.executor;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.bsim.friendlocator.data.Contact;
import com.bsim.friendlocator.restservices.executor.remote.RemoteUrls;
import com.bsim.friendlocator.restservices.executor.remote.ServerProxy;
import com.bsim.friendlocator.restservices.requestmessage.AcceptSession;
import com.bsim.friendlocator.restservices.requestmessage.CheckSession;
import com.bsim.friendlocator.restservices.requestmessage.CreateNewSession;
import com.bsim.friendlocator.restservices.requestmessage.DeleteSessionRequest;
import com.bsim.friendlocator.restservices.requestmessage.SendLocationRequest;
import com.bsim.friendlocator.restservices.requestmessage.UpdateMapRequest;
import com.bsim.friendlocator.restservices.responsemessage.AcceptSessionResponse;
import com.bsim.friendlocator.restservices.responsemessage.CheckSessionResponse;
import com.bsim.friendlocator.restservices.responsemessage.DeleteSessionResponse;
import com.bsim.friendlocator.restservices.responsemessage.NewSessionResponse;
import com.bsim.friendlocator.restservices.responsemessage.SendLocationResponse;
import com.bsim.friendlocator.restservices.responsemessage.UpdateMapResponse;


public class RemoteServer implements Server {

	@Override
	public NewSessionResponse createNewSession(CreateNewSession createNewSession) {
		NewSessionResponse response = new NewSessionResponse();
		String sessionName = createNewSession.getName();
		String sessionDescription = createNewSession.getDescription();
		String sessionId = null;
		if (sessionDescription == null || sessionDescription.trim().length() == 0) {
			sessionDescription = "default_description";
		}
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("name", sessionDescription));
		params.add(new BasicNameValuePair("description", "sample description"));
		JSONObject serverResponse = ServerProxy.createSession(sessionDescription, "sample description");
		int success = 0;
		String message = "";
		try {
			success = serverResponse.getInt("success");
			message = serverResponse.getString("message");
			sessionId = serverResponse.getString("message");
		} catch (JSONException e) {
			Log.e("serverexception", e.getMessage());
			e.printStackTrace();
		}
		if (success == 0) {
			response.setSuccess(false);
			return response;
		}
		
		List<Contact> contacts = createNewSession.getContacts();
		for (Contact contact: contacts) {
			
		}
		response.setSuccess(true);
		response.setSessionId(sessionId);
		response.setMessage("session created successfully");
		return response;
	}

	@Override
	public CheckSessionResponse checkSession(CheckSession checkSession) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public AcceptSessionResponse acceptSession(AcceptSession acceptSession) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public UpdateMapResponse updateMap(UpdateMapRequest updateMapRequest) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SendLocationResponse sendLocation(SendLocationRequest request) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public DeleteSessionResponse deleteSession(DeleteSessionRequest request) {
		// TODO Auto-generated method stub
		return null;
	}
}
