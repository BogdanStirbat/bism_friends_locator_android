package com.bsim.friendlocator.restservices.executor;

import com.bsim.friendlocator.restservices.requestmessage.AcceptSession;
import com.bsim.friendlocator.restservices.requestmessage.CheckSession;
import com.bsim.friendlocator.restservices.requestmessage.CreateNewSession;
import com.bsim.friendlocator.restservices.requestmessage.DeleteSessionRequest;
import com.bsim.friendlocator.restservices.requestmessage.SendLocationRequest;
import com.bsim.friendlocator.restservices.requestmessage.UpdateMapRequest;
import com.bsim.friendlocator.restservices.responsemessage.AcceptSessionResponse;
import com.bsim.friendlocator.restservices.responsemessage.CheckSessionResponse;
import com.bsim.friendlocator.restservices.responsemessage.DeleteSessionResponse;
import com.bsim.friendlocator.restservices.responsemessage.NewSessionResponse;
import com.bsim.friendlocator.restservices.responsemessage.SendLocationResponse;
import com.bsim.friendlocator.restservices.responsemessage.UpdateMapResponse;

public interface Server {
	
	public NewSessionResponse createNewSession(CreateNewSession createNewSession);
	
	public CheckSessionResponse checkSession(CheckSession checkSession);
	
	public AcceptSessionResponse acceptSession(AcceptSession acceptSession);
	
	public UpdateMapResponse updateMap(UpdateMapRequest updateMapRequest);
	
	public SendLocationResponse sendLocation(SendLocationRequest request);
	
	public DeleteSessionResponse deleteSession(DeleteSessionRequest request);
}
