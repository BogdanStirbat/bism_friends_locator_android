package com.bsim.friendlocator.constants;

public class SharedPreferenceConstants {
	public static final String FILE_NAME = "com.bsim.friendlocator.sharedpreferences";
	
	public static final String STATE = "STATE";
	
	public static final String SESSION_STARTED_BY_CURRENT_USER = "STARTED_BY_CURRENT_USER";
	
	public static final String SESSION_ID = "SESSION_ID";
	
	public static final String CURRENT_USER_NAME = "CURRENT_USER_NAME";
	
	public static final String CURRENT_USER_PHONE = "CURRENT_USER_PHONE";
}
