package com.bsim.friendlocator.activity;

import java.util.ArrayList;
import java.util.List;

import com.bsim.friendlocator.R;
import com.bsim.friendlocator.constants.ActivtyRequestCode;
import com.bsim.friendlocator.constants.SharedPreferenceConstants;
import com.bsim.friendlocator.data.Contact;
import com.bsim.friendlocator.restservices.executor.DummyServer;
import com.bsim.friendlocator.restservices.executor.RemoteServer;
import com.bsim.friendlocator.restservices.executor.Server;
import com.bsim.friendlocator.restservices.requestmessage.CreateNewSession;
import com.bsim.friendlocator.restservices.responsemessage.NewSessionResponse;

import android.support.v7.app.ActionBarActivity;
import android.support.v4.app.Fragment;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.provider.ContactsContract;

public class NewSessionActivity extends ActionBarActivity {
	
	private List<Contact> contacts;
	private ArrayList<String> displayContacts;
	private ArrayAdapter<String> adapter;
	private ListView friendsListView;
	
	private Server server;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_new_session);

		if (savedInstanceState == null) {
			getSupportFragmentManager().beginTransaction()
					.add(R.id.container, new PlaceholderFragment()).commit();
		}
		contacts = new ArrayList<Contact>();
		displayContacts = new ArrayList<String>();
		adapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.custom_listview, displayContacts);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.new_session, menu);
		return true;
	}
	
	@Override 
	public void onResume() {
		super.onResume();
		server = new RemoteServer();
	}
	
	@Override
	public void onStart() {
		super.onStart();
		friendsListView = (ListView) findViewById(R.id.friendsInSessionList);
		friendsListView.setAdapter(adapter);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_new_session,
					container, false);
			return rootView;
		}
	}
	
	private void addContact(Contact contact) {
		String displayedContact = contact.getName() + ": " + contact.getPhoneNumber();
		contacts.add(contact);
		displayContacts.add(displayedContact);
		adapter.notifyDataSetChanged();
	}
	
	@Override
	public void onActivityResult(int reqCode, int resultCode, Intent data) {
		super.onActivityResult(reqCode, resultCode, data);
		if (ActivtyRequestCode.PICK_CONTACT == reqCode) {
			if (resultCode == Activity.RESULT_OK) {
				Uri contactData = data.getData();
				Cursor c =  getContentResolver().query(contactData, null, null, null, null);
				if (c.getCount() > 0) {
					while(c.moveToNext()) {
						String contactId = c.getString(c.getColumnIndex(ContactsContract.Contacts._ID));
						String name = c.getString(c.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
						String phoneNumber = "";
						String hasPhone = c.getString(c.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));
						if ( hasPhone.equalsIgnoreCase("1")) {
							hasPhone = "true";
						} else {
							hasPhone = "false";
						}
						if (Boolean.parseBoolean(hasPhone)) {
							Cursor phones = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,ContactsContract.CommonDataKinds.Phone.CONTACT_ID +" = "+ contactId, null, null);
							while (phones.moveToNext()) {
								phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
							}
							phones.close();
						}
						
						if (isStringFilled(name) && isStringFilled(phoneNumber)) {
							Contact contact = new Contact();
							contact.setName(name);
							contact.setPhoneNumber(removeNonNumericCharacters(phoneNumber));
							addContact(contact);
						}
					}
				}
				c.close();
			}
		}
	}
	
	private boolean isStringFilled(String target) {
		if (target != null && target.trim().length() > 0) {
			return true;
		}
		return false;
	}
	
	private String removeNonNumericCharacters(String phoneNumber) {
		if (!isStringFilled(phoneNumber)) {
			return "";
		}
		String result = phoneNumber;
		result = result.replaceAll("[^\\d]", "");
		return result;
	}
	
	public void addNewContactToSession(View view) {
		Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
		startActivityForResult(intent, ActivtyRequestCode.PICK_CONTACT);
	}
	
	private CreateNewSession constructRequest() {
		EditText descriptionEdit = (EditText) findViewById(R.id.addDescriptionText);
		String description = descriptionEdit.getText().toString();
		SharedPreferences settings = getSharedPreferences(SharedPreferenceConstants.FILE_NAME, Context.MODE_PRIVATE);
		String username = settings.getString(SharedPreferenceConstants.CURRENT_USER_NAME, null);
		String phoneNumber = settings.getString(SharedPreferenceConstants.CURRENT_USER_PHONE, null);
		CreateNewSession createNewSession = new CreateNewSession();
		createNewSession.setContacts(contacts);
		createNewSession.setName(username);
		createNewSession.setPhone(phoneNumber);
		createNewSession.setDescription(description);
		return createNewSession;
	}
	
	private NewSessionResponse createSessionOnServer() {
		CreateNewSession createNewSession = constructRequest();
		NewSessionResponse response = server.createNewSession(createNewSession);
		return response;
	}
	
	public void okCreateNewSession(View view) {
		CreateSessionTask asynTask = new CreateSessionTask(NewSessionActivity.this);
		asynTask.execute();
	}
	
	public void cancelCreateNewSession(View view) {
		Intent returnIntent = new Intent();
		returnIntent.putExtra("result", "cancelled");
		setResult(RESULT_CANCELED, returnIntent);
		finish();
	}
	
	class CreateSessionTask extends AsyncTask {
		
		NewSessionActivity mActivity;
		
		public CreateSessionTask(NewSessionActivity activity) {
			super();
			this.mActivity = activity;
		}
 
		@Override
		protected Object doInBackground(Object... params) {
			CreateNewSession createNewSession = constructRequest();
			NewSessionResponse response = server.createNewSession(createNewSession);
			int result;
			if (response.isSuccess()) {
				result = RESULT_OK;
				
			} else {
				result = RESULT_CANCELED;
			}
			Intent returnIntent = new Intent();
			returnIntent.putExtra("message", response.getMessage());
			mActivity.setResult(result, returnIntent);
			mActivity.finish();
			return null;
		}
		
	}
}
